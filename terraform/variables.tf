variable "name" {
  type        = string
  description = "Name for Page deployment"
}

variable "verification_code_primary" {
  type        = string
  description = "DNS verification code"
}

variable "verification_code_secondary" {
  type        = string
  description = "DNS verification code"
}

variable "fathom_cdn" {
  type        = string
  description = "Fathom CDN"
}
