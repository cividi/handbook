terraform {
  required_version = ">= 1.0"
}

module "gitlab-pages-dns-primary" {
  source  = "gitlab.com/cividi/gitlab-pages-dns/local"
  version = "~> 1.2.0"

  zone_id           = "6a4745a4b0b49b2889ab3f3a2aed8650"
  name              = var.name
  verification_code = var.verification_code_primary
  fathom_cdn        = var.fathom_cdn
}

module "gitlab-pages-dns-alt" {
  source  = "gitlab.com/cividi/gitlab-pages-dns/local"
  version = "~> 1.2.0"

  zone_id           = "8c249d058f2154779cdde148a3bbe512"
  name              = var.name
  verification_code = var.verification_code_secondary
  fathom_cdn        = var.fathom_cdn
  secondary         = true
}
