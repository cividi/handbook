# File Formats

## GTFS - General Transit Feed Specification

The most common openely available data for public transport is structured as [GTFS](https://gtfs.org) (and its real-time sibling GTFS-RT). Initially developed by Google to standardize public transport routes and timetables it has become the de facto standard in published schedule and routing data for public transport.

### What is GTFS

At it's core each GTFS dataset has a couple of .txt files containing [CSV](/data/file_formats#csv) data.

For a full overview, see [GTFS documentation on schedules](https://gtfs.org/schedule/examples/routes-stops-trips/). The main data types to be aware of: `Routes`, `Stops`, `Trips` and `Service Exceptions`.

???+ note "Details"

    === "Routes"
        - `agency.txt`: List of local operaters, e.g. SBB, VBZ, Postauto
        - `routes.txt`: List of serviced routes, including a type (0 = tram, 2 = rail, 3 = bus, more see [docs](https://gtfs.org/schedule/reference/#routestxt)) name and color
        - `transfer.txt`: List of possible transfers in the network
    === "Stops"
        - `stops.txt`: List of individual stops, inlcuding a lat/lng location, can be parts of larger parent stations
        - `stop_times.txt`: List of arrival and departure times
        - `frequencies.txt`: List of sequences of regular service for a trip_id used instead of stop times
    === "Trips"
        - `calendar.txt`: List of sets of "service days", e.g. Mo, We, Fr from March to September
        - `trips.txt`: mapping routes.txt to calender.txt with a direction and shape
        - `shapes.txt`: List of waypoints for a route
    === "Service exceptions"
        - `calendar_dates.txt`: List of special operation days (e.g. holidays)

### Tools

There are many tools out there to work with GTFS data. Heres a few to get you started

| Name     | Language                           | Description                                            |                                                                             |                                                                         |                                                                      |
| -------- | ---------------------------------- | ------------------------------------------------------ | --------------------------------------------------------------------------- | ----------------------------------------------------------------------- | -------------------------------------------------------------------- |
| GTFS Kit | :fontawesome-brands-python: Python | Powerful toolkit to load, modify and analyse GTFS data | [:octicons-package-dependents-16: PyPi](https://pypi.org/project/gtfs-kit/) | [:fontawesome-solid-code: Gitlab](https://gitlab.com/mrcagney/gtfs_kit) | [:octicons-book-16: Docs](https://mrcagney.gitlab.io/gtfs_kit_docs/) |

### Working with Swiss GTFS

!!! info "Data download"

    The data is available via [:octicons-download-16: opentransportdata.swiss](https://opentransportdata.swiss/en/group/timetables-gtfs).

Switzerland publishes one GTFS feed for the entire country. While this is convenient to fetch everything at once, it makes handling the data potentially hard and memory consuming.

#### Only using a subset

!!! warning

    Don't use this if this removes data that is relevant to your analysis. E.g. if connections in the whole network are relevant.

If you're calculations are scoped to a certain region consider slicing the data before using it. This reduces the overall information density - especially if non of the big urban centers (Zurich, Geneva, Basel, Bern, etc.) are not involved.

You can use GTFS-Kit to produce a copy of any GTFS Feed.

!!! example "Code"

    === "Python"

        Make sure dependencies are installed.

        ```bash
        pip install gtfs-kit requests python-slugify
        ```
        ```python
        # notebooks/extract_gtfs_subset.ipynb

        import sys
        import requests
        import zipfile
        from slugify import slugify
        import gtfs_kit as gk
        from pathlib import Path
        import geopandas as gpd

        # Set up data directory
        DIR = Path('..')
        sys.path.append(str(DIR))
        DATA_DIR = DIR/'data/'

        # download helper
        def download_data(url):
          req = requests.get(url)
          filename = url.split('/')[-1]
          with open(f'{DATA_DIR}/{filename}','wb') as output_file:
            output_file.write(req.content)
          return filename

        # Download feed
        gtfs_file_name = download_data('https://opentransportdata.swiss/en/dataset/timetable-2021-gtfs2020/resource_permalink/gtfs_fp2021_2021-01-27.zip')

        # Load feed file
        gtfs_path = f'DATA_DIR/{gtfs_file_name}'
        feed = gk.read_feed(path, dist_units='km')

        # Calculate stats
        feed.describe()

        # Download swissBOUNDARIES3D to extract cantonal shape
        file_name = download_data('https://data.geo.admin.ch/ch.swisstopo.swissboundaries3d/swissboundaries3d_2022-05/swissboundaries3d_2022-05_2056_5728.gdb.zip')
        unzipped_file_name = file_name[:-4]

        try:
          with zipfile.ZipFile(file_name) as z:
              with open(unzipped_file_name, 'wb') as f:
                  f.write(z.read(file_name))
        except:
            print("Invalid file")

        # Load shape, e.g. a canton
        SELECTED_CANTON = "Ticino"

        cantons_path = f'{DATA_DIR}/{unzipped_file_name}'
        cantons = gpd.read_file(cantons_path, layer='TLM_KANTONSGEBIET')
        shape = cantons.loc[cantons['NAME'] == "Ticino"][["geometry"]]

        # Show selected canton geometry
        shape

        # Calculate sub feed and updated stats
        subset_feed = feed.restrict_to_area(shape)
        subset_feed.describe()

        # Save sub feed to disk
        out_file = f'{DATA_DIR}/{gtfs_file_name[:-4]}-only-{slugify(SELECTED_CANTON)}.zip'
        subset_feed.write(out_file)
        ```
