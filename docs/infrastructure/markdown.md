# Markdown

[Markdown](https://daringfireball.net/projects/markdown/) is a common syntax for writing technical and non-technical documents, invented by John Gruber. Developers use it to write documentation, writers and journalists use it to publish books and articles in blogs or newspapers, scientists use it to publish papers.

It is a very lightweight syntax to add structure to your text and easy to learn. This very handbook is written in Markdown. You can see the underlying "source code" if you click on the pencil in the top right corner of this page. The full syntax reference is available at [daringfireball.net/projects/markdown/syntax](https://daringfireball.net/projects/markdown/syntax).

TL;DR: The main elements to get started are:

## The basics

### Headings

The main structure of a document is its outline. The outline in turn can be created by defining the heading with different heading levels. Markdown uses the `#` to signify headings. The number of pound signs at the begining of the line correspond to the level of the heading itself. `# Title` is a level one heading, or the main title of the document. `## Section 1` is a level two heading.

### Bold and italics

To highlight text inline **bold** and _italics_ can be used.

=== "Markdown"

    ```md
    **bold text** and _italics_
    ```

=== "Preview"

    **bold text** and _italics_

### Lists

There can be unordered lists

=== "Markdown"

    ```md
    - Item 1
    - Item 2
    - Item 3
    ```

=== "Preview"

    - Item 1
    - Item 2
    - Item 3

or ordered lists

=== "Markdown"

    ```md
    1. Item 1
    1. Item 2
    1. Item 3
    ```

=== "Preview"

    1. Item 1
    1. Item 2
    1. Item 3

### Quotes

To add a blockquote:

=== "Markdown"

    ```md
    > My quote
    ```

=== "Preview"

    > My quote

### Links

In order to link to other pages (e.g. on the same page) or other pages in the internet entirely the following syntax is used:

=== "Markdown"

    ```md
    [text to be linked to external content](https://linktoadifferentpage.example/conent)
    ```

    ```md
    [The handbook](./handbook.md)
    ```

=== "Preview"

    [text to be linked to external content](https://linktoadifferentpage.example/conent)

    [The handbook](./handbook.md)

### Code

To include code snippets, inlcuding syntax highlighting add the following:

=== "Markdown"

    ````md
    ```python
    from frictionless import Resource

    # Load data
    resource = Resource("awesome-data.csv")

    # Display data
    print(resource.to_petl())
    ```
    ````

=== "Preview"

    ```python
    from frictionless import Resource

    # Load data
    resource = Resource("awesome-data.csv")

    # Display data
    print(resource.to_petl())
    ```
