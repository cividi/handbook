# Version Control

We use version control - specifically [git](https://git-scm.com) - to manage documents, source code and other artifacts. [GitLab](https://about.gitlab.com) is our main version control management system.

## What is git?

TODO: small introduction to git.

## How to open a Merge Request

TODO: small introduction to merge requests.

## Semantic Versioning and branches

In projects where versions are used we adhere to the [Semantic Versioning](https://semver.org) specification. To make releases and changelog generation easy we recommend to use [Conventional Commits (v1.0.0)](https://www.conventionalcommits.org/en/v1.0.0/) and branches. Always commit first to a branch and create merge request based on the branch.

### TL;DR

- Commit messages starting with `fix: ` trigger a patch version bump
- Commit messages starting with `feat: ` trigger a minor version bump
- Commit messages including `BREAKING CHANGE: ` in the footer and or a `!` after the type/scope (e.g. `feat(api)!: `) trigger a major version bump.
- Commit messages can be scoped with adding brackets, e.g. `feat(frontend): ` scopes the feature to a frontend section (e.g. in a genrerated changelog)
- Other options (based on the [Angular convention](https://github.com/angular/angular/blob/22b96b9/CONTRIBUTING.md#-commit-message-guidelines)) for prefixes: `docs: `, `style: `, `test: `, `refactor: `, `build`, `ci: `, `chore: `, `perf: `

## Automatic versioning

If configured, this enables each push to `main` to trigger a [`semantic-release`](https://semantic-release.gitbook.io/semantic-release/)
CI job that determines and pushes a new version tag (if any) based on the
last version tagged and the new commits pushed. Notice that this means that if a
Merge Request contains, for example, several `feat: ` commits, only one minor
version bump will occur on merge. If your Merge Request includes several commits
you may prefer to ignore the prefix on each individual commit and instead add
an empty commit sumarizing your changes like so:

```
git commit --allow-empty -m '[BREAKING CHANGE|feat|fix]: <changelog summary message>'
```

### Configure automatic versioning

To enabale automatic versioning, start commiting with conventional commits style commit messages as outlined above, then add a `GITLAB_TOKEN` and the following `.gitlab-ci.yml` job to your project:

```yaml
include:
  # include auto release jobs
  - remote: https://gitlab.com/cividi/devops/pipelines/-/raw/main/pipelines/release.gitlab-ci.yml

stages:
  - test
  - release
```

Now every merge onto main will automatically run a release pipeline and create a git tag and GitLab release. Currently no CHANGELOG.md is pushed automatically.
