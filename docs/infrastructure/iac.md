# Infrastructure as Code

All infrastructure should be managed as code and versioned in git. Each project should have it's own setup to help decentralize access and resilience. To ease setup, central repositories at [gitlab.com/cividi/devops](https://gitlab.com/cividi/devops) for modules (e.g. terraform) and templates (e.g. gitlab-ci.yml) contain common patterns and architectures.

## Terraform

### What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale infrastructure. It's provider agnostic. You're encouraged to read the documentation as well as [Terraform: Up and Running](https://www.oreilly.com/library/view/terraform-up-and/9781491977071/).

## How to use Terraform

There are three pieces we use to manage our infrastructure:

1. The [GitLab provided Terraform Backend](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html) to store state
1. [Custom Terraform Modules](https://gitlab.com/cividi/devops/terraform-modules) to standardize and simplify setting up infrastructure
1. A custom [GitLab CI-Pipeline template](https://gitlab.com/cividi/devops/pipelines/-/blob/main/pipelines/terraform.gitlab-ci.yml) to verify/test and deploy terraform plans

To see how these pieces work together, have a look at this handbooks' own setup.

- [`.gitlab-ci.yml`](https://gitlab.com/cividi/handbook/-/blob/main/.gitlab-ci.yml) to include the [terraform template](https://gitlab.com/cividi/devops/pipelines/-/blob/main/pipelines/terraform.gitlab-ci.yml)
- [`terraform/main.tf`](https://gitlab.com/cividi/handbook/-/blob/main/terraform/main.tf) to specify the modules and resources, combined with ci/cd variables for tokens for each provider
- [`terraform/backend.tf`](https://gitlab.com/cividi/handbook/-/blob/main/terraform/backend.tf) to initialize and run on the GitLab Terraform backend

## Setup managed backend

!!! note

    Also see GitLab docs on [managed Terraform state](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html)

Main steps:

1. add a `backend.tf` containing:
   ```hcl
   terraform {
     backend "http" {
     }
   }
   ```
1. add a `gitlab-ci.yml` containing:
   ```yaml
   include:
     remote: https://gitlab.com/cividi/devops/pipelines/-/raw/main/pipelines/terraform.gitlab-ci.yml
   ```
1. exclude temporary terraform files in your `.gitignore`

   ```gitignore
   **/.terraform/*

   # .tfstate files
   *.tfstate
   *.tfstate.*

   # Crash log files
   crash.log
   crash.*.log

   # Exclude all .tfvars files, which are likely to contain sensitive data, such as
   # password, private keys, and other secrets. These should not be part of version
   # control as they are data points which are potentially sensitive and subject
   # to change depending on the environment.
   *.tfvars
   *.tfvars.json

   # Ignore override files as they are usually used to override resources locally and so
   # are not checked in
   override.tf
   override.tf.json
   *_override.tf
   *_override.tf.json

   # Include override files you do wish to add to version control using negated pattern
   # !example_override.tf

   # Include tfplan files to ignore the plan output of command: terraform plan -out=tfplan
   # example: *tfplan*

   # Ignore CLI configuration files
   .terraformrc
   terraform.rc
   ```

1. commit changes on a new branch
1. create and merge a merge request

## Access managed backend locally

Copy a pre-populated Terraform init command:

1. On the top bar in GitLab, select Main menu > Projects and find your project.
1. On the left sidebar, select Infrastructure > Terraform.
1. Next to the environment you want to use, select Actions () and select Copy Terraform init command.
