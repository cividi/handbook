# Handbook

This handbook is part of the [cividi infrastructure](/infrastructure/). It is meant as a quick reference and guide to company, data and other matters. Anyone can (and should) contribute. How do can I do that, you may ask. Here's how:

## How to edit

In order to edit any existing page

1.  Navigate to the page you'd like to edit
1.  Click the :material-pencil: pencil icon on the top right corner of the page
1.  Edit the file inside GitLabs editor, formatted in [Markdown](../markdown/)
1.  Write a short desciption commit message with the [format](../version_control/#semantic-versioning-and-branches): `docs: <YOUR MESSAGE>` or `fix: <YOUR MESSAGE>`

    !!! warning "Important"

        You have to prefix the message with `docs: ` or `fix: `, otherwise no new version and hence no new publication will happen

1.  Rename the target branch (not `main`, no spaces, uppercase or special characters): e.g. `add-file-format-csv`

    !!! warning "Important"

        Do not commit to `main`

1.  Enable `Start a new merge request with these changes`
1.  Commit your changes
1.  On the now open [Merge Request](../version_control/#how-to-open-a-merge-request) assign a reviewer

Once your proposal is approved it will be merged and automatically published.

Congrats, you just successfully edited a page in the handbook. See below on how to start a new page and more advanced editing options.

## How to add a new page

1.  Go to [gitlab.com/cividi/handbook/-/tree/main/docs](https://gitlab.com/cividi/handbook/-/tree/main/docs)
1.  Navigate to the subsection folder you want to add a page to
1.  Click on the plus button above the listing
1.  Choose `New file`
1.  Choose a name with no spaces, uppercases or special characters and add `.md` at the end, e.g. `holidays.md`
1.  Write your entry in [Markdown](../markdown/) starting with a level one heading
1.  Write a short desciption commit message with the [format](../version_control/#semantic-versioning-and-branches): `docs: <YOUR MESSAGE>` or `fix: <YOUR MESSAGE>`

    !!! warning "Important"

        You have to prefix the message with `docs: ` or `fix: `, otherwise no new version and hence no new publication will happen

1.  Rename the target branch (not `main`, no spaces, uppercase or special characters): e.g. `add-file-format-csv`

    !!! warning "Important"

        Do not commit to `main`

1.  Enable `Start a new merge request with these changes`
1.  Commit your changes
1.  On the now open [Merge Request](../version_control/#how-to-open-a-merge-request) assign a reviewer

Once your proposal is approved it will be merged and automatically published.

Congrats, you just successfully edited a page in the handbook. See above on how to edit an existing page and read further for more advanced editing options.

## Advanced editing

### Images

To include images in your pages, you first need to upload them to the `docs/images/` folder via git or GitLab. Then you can reference them with [Markdown](../markdown/) like this

```md
![Alt Image Text](/images/my_image.jpg "Image Title")
```

### Info Boxes

To include (collapsable) info boxes like these:

!!! quote

    > Two things are infinite: the universe and human stupidity; and I'm not sure about the universe.
    ― Albert Einstein

Either [read the documentation](https://squidfunk.github.io/mkdocs-material/reference/admonitions/) or copy the following example for a simple warning box:

=== "Markdown"

    ```md
    !!! warning

        Do not try this at home!
    ```

=== "Preview"

    !!! warning

        Do not try this at home!

Or a callapsable example box:

=== "Markdown"

    ```md
    ???+ example

        Rather try this...
    ```

=== "Preview"

    ???+ example

        Rather try this...

### Tabs

To [seperate content into tabs](https://squidfunk.github.io/mkdocs-material/reference/content-tabs/) you can use the following syntax:

=== "Markdown"

    ```md
    === "Tab 1"

        Content Tab 1
    === "Tab 2"

        Content Tab 2
    ```

=== "Preview"

    === "Tab 1"

        Content Tab 1
    === "Tab 2"

        Content Tab 2
